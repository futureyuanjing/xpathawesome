﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace XpathAwesome
{
    class Program
    {
        public static List<SiteConfig> list_siteconfig;
        public static string configName="intel.xml";
        private static void GetSiteConfig(int websiteID = -1)
        {
            var sitexml = AppDomain.CurrentDomain.BaseDirectory + configName;
            list_siteconfig = new List<SiteConfig>();
            try
            {
                using (var sr = new StreamReader(sitexml))
                {
                    var str = sr.ReadToEnd();
                    list_siteconfig = CommonHelper.Deserialize(typeof(List<SiteConfig>), str) as List<SiteConfig>;
                }
                Console.WriteLine("{0} is loaded to config list", configName);
                if (websiteID != -1)
                    list_siteconfig = list_siteconfig.Where(item => item.WebsiteID == websiteID).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("cannot load xml correctly", ex);
            }
        }
        static void Main(string[] args)
        {
            GetSiteConfig();
            string html;
            //string xpath = list_siteconfig[0].ItemsConfigs[0].ToString();
            //Console.WriteLine(xpath);
        }
    }
}
