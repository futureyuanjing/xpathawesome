﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XpathAwesome
{
    [Serializable()]
    [XmlRoot("SiteConfig")]
    public class SiteConfig
    {
        public int SpiderVersion { get; set; }
        public string Ioc { get; set; }
        public int WebsiteID { get; set; }
        public string SiteUrl { get; set; }
        public string SiteName { get; set; }

        public string SearchUrl { get; set; }

        
        public string ItemLocation { get; set; }

        [XmlArray("ItemsConfig"), XmlArrayItem("FieldConfig", typeof(FieldConfig))]
        public List<FieldConfig> ItemsConfigs { get; set; }
        
        [XmlArray("DetailConfig"), XmlArrayItem("FieldConfig", typeof(FieldConfig))]
        public List<FieldConfig> DetailConfigs { get; set; }

    }

    [Serializable()]
    [XmlRoot("FieldConfig")]
    public class FieldConfig
    {
       
        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlText]
        public string xpath { get; set; }

    }

}
